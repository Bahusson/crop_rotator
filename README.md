Program "Płodozmian" ("Crop Rotator") został stworzony z myślą o ułatwieniu
osobom zajmującym się zawodowo i amatorsko rolą, stosowania dobrych praktyk
rolniczych w zakresie płodozmianu, międzyplonów, oraz rolnictwa ekologicznego.

W czasach gdy przy obecnej rabunkowej gospodarce ziemi uprawnej
pozostało nam na 60 lat, a przy galopującej katastrofie klimatycznej jedyną
sprawdzoną opcją na wychwyt CO2 jest wiązanie go w glebie* i w biomasie,
trzeba tym bardziej postawić na łatwość w zdobywaniu i stosowaniu wiedzy
rolniczej.

Ten bezpłatny program jest zarazem moją pracą dyplomową na SGGW na kierunku
Rolnictwo, który to muszę ukończyć aby móc samemu również wziąć się za uprawę
a zarazem naprawę, zniszczonej przez rewolucję przemysłową ziemi.

Niech ta aplikacja będzie moją cegiełką, która posłuży do odbudowy świata,
z którą z nieznanych mi przyczyn zwlekaliśmy do ostatniej chwili.

W tym miejscu pojawi się w najbliższym czasie adres internetowy wersji testowej.
Naturalnie zapraszam do testowania i dzielenia się opiniami wszystkich zainteresowanych koleżanki i kolegów rolników.

Uwagi prośby i zażalenia proszę kierować na adres: j.kozdrowicz@apdeveloper.com.pl



*Niestety jak na razie jest to jedyny skalowalny sposób możliwy do wprowadzenia
na JUŻ. Inne rozwiązania nie otrzymały wciąż wystarczającego finansowania,
wymagają długiego i kosztownego wdrożenia (emisje) i często są niedostatecznie przetestowane. Ucieczka na księżyc i/lub marsa, oraz obszary pokryte do tej pory wieczną zmarzliną to ułańska fantazja a nie realna opcja. Także albo my, albo
ryzykujemy geoinżynierię, która może się nam wymknąć spod kontroli. :/
