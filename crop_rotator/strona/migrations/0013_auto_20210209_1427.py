# Generated by Django 3.1.5 on 2021-02-09 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('strona', '0012_auto_20210209_1400'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagenames',
            name='all_plans',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='pagenames',
            name='all_plans_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='pagenames',
            name='all_plans_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='allelopatic_conflict',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='allelopatic_conflict_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='allelopatic_conflict_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='collides',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='collides_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='collides_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='harms',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='harms_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='harms_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='in_step',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='in_step_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='in_step_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='notes',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='notes_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='notes_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='well_cooperates',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='well_cooperates_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='well_cooperates_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='pagenames',
            name='see_more',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='pagenames',
            name='see_more_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='pagenames',
            name='see_more_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
