# Generated by Django 3.1.5 on 2021-02-08 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('strona', '0006_auto_20210205_1136'),
    ]

    operations = [
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='error_len',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='error_len_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='error_len_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='fabs_and',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='fabs_and_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='fabs_and_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='in_this_plan',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='in_this_plan_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='in_this_plan_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='len_required',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='len_required_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='len_required_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='option_select',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='option_select_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='option_select_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='remove_or_add',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='remove_or_add_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='remove_or_add_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='should_be_fabs',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='should_be_fabs_en',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='rotatoreditorpagenames',
            name='should_be_fabs_pl',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
