# Generated by Django 3.1.3 on 2020-12-16 17:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AboutPageNames',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('about_project', models.TextField()),
                ('about_project_pl', models.TextField(null=True)),
                ('about_project_en', models.TextField(null=True)),
                ('send_email', models.CharField(max_length=200)),
                ('send_email_pl', models.CharField(max_length=200, null=True)),
                ('send_email_en', models.CharField(max_length=200, null=True)),
                ('gitter', models.CharField(max_length=200)),
                ('gitter_pl', models.CharField(max_length=200, null=True)),
                ('gitter_en', models.CharField(max_length=200, null=True)),
                ('github', models.CharField(max_length=200)),
                ('github_pl', models.CharField(max_length=200, null=True)),
                ('github_en', models.CharField(max_length=200, null=True)),
                ('login_to_see', models.CharField(max_length=200)),
                ('login_to_see_pl', models.CharField(max_length=200, null=True)),
                ('login_to_see_en', models.CharField(max_length=200, null=True)),
            ],
            options={
                'verbose_name_plural': 'Registry Names',
            },
        ),
        migrations.CreateModel(
            name='PageNames',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lang_flag', models.ImageField(upload_to='images')),
                ('lang_flag_pl', models.ImageField(null=True, upload_to='images')),
                ('lang_flag_en', models.ImageField(null=True, upload_to='images')),
                ('headtitle', models.CharField(max_length=200)),
                ('headtitle_pl', models.CharField(max_length=200, null=True)),
                ('headtitle_en', models.CharField(max_length=200, null=True)),
                ('mainpage', models.CharField(max_length=200)),
                ('mainpage_pl', models.CharField(max_length=200, null=True)),
                ('mainpage_en', models.CharField(max_length=200, null=True)),
                ('about', models.CharField(max_length=200)),
                ('about_pl', models.CharField(max_length=200, null=True)),
                ('about_en', models.CharField(max_length=200, null=True)),
                ('contact', models.CharField(max_length=200)),
                ('contact_pl', models.CharField(max_length=200, null=True)),
                ('contact_en', models.CharField(max_length=200, null=True)),
                ('logout', models.CharField(max_length=200)),
                ('logout_pl', models.CharField(max_length=200, null=True)),
                ('logout_en', models.CharField(max_length=200, null=True)),
                ('login', models.CharField(max_length=200)),
                ('login_pl', models.CharField(max_length=200, null=True)),
                ('login_en', models.CharField(max_length=200, null=True)),
                ('register', models.CharField(max_length=50)),
                ('register_pl', models.CharField(max_length=50, null=True)),
                ('register_en', models.CharField(max_length=50, null=True)),
                ('see_more', models.CharField(max_length=200)),
                ('see_more_pl', models.CharField(max_length=200, null=True)),
                ('see_more_en', models.CharField(max_length=200, null=True)),
                ('editme', models.CharField(max_length=200)),
                ('editme_pl', models.CharField(max_length=200, null=True)),
                ('editme_en', models.CharField(max_length=200, null=True)),
            ],
            options={
                'verbose_name_plural': 'Page Names',
            },
        ),
        migrations.CreateModel(
            name='PageSkin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('themetitle', models.CharField(max_length=200)),
                ('position', models.IntegerField()),
                ('planimagedefault', models.ImageField(blank=True, null=True, upload_to='skins')),
                ('rotatorlogo_main', models.ImageField(blank=True, null=True, upload_to='skins')),
            ],
            options={
                'verbose_name_plural': 'Page Skins',
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='RegNames',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=50, null=True)),
                ('password_pl', models.CharField(max_length=50, null=True)),
                ('password_en', models.CharField(max_length=50, null=True)),
                ('re_password', models.CharField(max_length=50, null=True)),
                ('re_password_pl', models.CharField(max_length=50, null=True)),
                ('re_password_en', models.CharField(max_length=50, null=True)),
                ('name', models.CharField(max_length=50, null=True)),
                ('name_pl', models.CharField(max_length=50, null=True)),
                ('name_en', models.CharField(max_length=50, null=True)),
            ],
            options={
                'verbose_name_plural': 'Registry Names',
            },
        ),
    ]
