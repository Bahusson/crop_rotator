# Generated by Django 3.1.3 on 2020-12-05 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rotator', '0006_auto_20201205_1544'),
    ]

    operations = [
        migrations.AddField(
            model_name='crop',
            name='is_deep_roots',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='crop',
            name='is_leaves_mess',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='cropfamily',
            name='is_mandatory_crop',
            field=models.BooleanField(default=False),
        ),
    ]
