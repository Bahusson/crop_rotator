# Generated by Django 3.1.5 on 2021-02-16 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rotator', '0049_auto_20210216_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='crop',
            name='crop_relationships',
            field=models.ManyToManyField(blank=True, related_name='known_crops_interactions', to='rotator.CropsInteraction'),
        ),
    ]
