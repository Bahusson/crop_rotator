# Generated by Django 3.1.3 on 2021-01-02 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rotator', '0026_auto_20210102_1052'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='crop',
            name='allelopatic_to_tag',
        ),
        migrations.RemoveField(
            model_name='crop',
            name='synergic_to_tag',
        ),
        migrations.AddField(
            model_name='crop',
            name='bad_before',
            field=models.ManyToManyField(blank=True, related_name='bad_before_set', to='rotator.Crop'),
        ),
        migrations.AddField(
            model_name='crop',
            name='good_before',
            field=models.ManyToManyField(blank=True, related_name='good_before_set', to='rotator.Crop'),
        ),
    ]
