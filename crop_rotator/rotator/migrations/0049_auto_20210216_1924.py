# Generated by Django 3.1.5 on 2021-02-16 18:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rotator', '0048_auto_20210210_1348'),
    ]

    operations = [
        migrations.CreateModel(
            name='CropsInteraction',
            fields=[
                ('cropinteraction_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='rotator.cropinteraction')),
            ],
            options={
                'ordering': ['title'],
            },
            bases=('rotator.cropinteraction',),
        ),
        migrations.CreateModel(
            name='TagsInteraction',
            fields=[
                ('cropinteraction_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='rotator.cropinteraction')),
            ],
            options={
                'ordering': ['title'],
            },
            bases=('rotator.cropinteraction',),
        ),
        migrations.AddField(
            model_name='cropinteraction',
            name='about_tag',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tag_interaction_set', to='rotator.croptag'),
        ),
        migrations.AddField(
            model_name='croptag',
            name='crop_relationships',
            field=models.ManyToManyField(blank=True, related_name='known_tags_interactions', to='rotator.TagsInteraction'),
        ),
    ]
