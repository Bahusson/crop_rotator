# Generated by Django 3.1.3 on 2020-12-19 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rotator', '0019_auto_20201218_1523'),
    ]

    operations = [
        migrations.AddField(
            model_name='crop',
            name='descr_en',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='crop',
            name='descr_pl',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='crop',
            name='name_en',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='crop',
            name='name_pl',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='cropfamily',
            name='name_en',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='cropfamily',
            name='name_pl',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='cropmix',
            name='descr_en',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='cropmix',
            name='descr_pl',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='cropmix',
            name='name_en',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='cropmix',
            name='name_pl',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='rotationplan',
            name='title_en',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='rotationplan',
            name='title_pl',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='rotationstep',
            name='descr_en',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='rotationstep',
            name='descr_pl',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='rotationstep',
            name='title_en',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='rotationstep',
            name='title_pl',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
