# Generated by Django 3.1.3 on 2020-12-08 15:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rotator', '0013_auto_20201208_1611'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='crop',
            name='tags',
        ),
    ]
